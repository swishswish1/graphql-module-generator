import * as utils from './utils';

export const NL = '\r\n';
export const TABS = '\t\t\t';
export const suffixResolverClass = 'Resolver';
export const ReplaceToken = '/*ReplaceToken*/';

const useGuard = `  @UseGuards(GqlAuthGuard, TlsGuard)${NL}`;
const useDurationInterceptor = `  @UseInterceptors(DurationInterceptor)${NL}`;
const useUserValidationInterceptor = `  @UseInterceptors(new ExecutionContextValidationInterceptor(new BaseExecutionContextValidator()))${NL}`;

let code = '';

export interface IResolverEntry {
    genCode(parent: string): void;
}

export class BaseEntry {
    constructor(protected entry: any) { }

    protected f() {
        let args = '';
        let argsRealType = '';
        for (let arg of this.entry.arguments) {
            const argType = utils.isListField(arg) ? 'any[]' : 'any';
            args += `, @Args('${arg.name}') ${arg.name}: ${argType}`;
            argsRealType += `, {${arg.name}: ${arg.types}}`;
        }

        if (args.length > 2) {
            args = args.slice(2);
            argsRealType = argsRealType.slice(2);
        }

        return { args, argsRealType };
    }
}

export class ResolverQueryEntry extends BaseEntry implements IResolverEntry {
    constructor(entry: any) {
        super(entry);
    }

    genCode(parent: string): void {
        const fr = this.f();
        const innerCode =
            `${NL}` +
            `  // Args:   ${fr.argsRealType}${NL}` +
            `  // Return: ${this.entry.types}${NL}` +
            `  @Query()${NL}` +
            useGuard +
            useDurationInterceptor +
            useUserValidationInterceptor +
            `  async ${this.entry.name}(${fr.args}) {${NL}` +
            `${NL}` +
            `    return [];  //@@${NL}` +
            `  }${NL}` +
            `${ReplaceToken}`;
        code = code.replace(`${ReplaceToken}`, innerCode);
    }
}

export class ResolverMutationEntry  extends BaseEntry implements IResolverEntry {
    constructor(entry: any) {
        super(entry);
    }

    genCode(parent: string): void {
        const fr = this.f();
        const innerCode =
            `${NL}` +
            `  // Args:   ${fr.argsRealType}${NL}` +
            `  // Return: ${this.entry.types}${NL}` +
            `  @Mutation()${NL}` +
            useGuard +
            useDurationInterceptor +
            useUserValidationInterceptor +
            `  async ${this.entry.name}(${fr.args}): Promise<string> {${NL}` +
            `${NL}` +
            `    return '';  //@@${NL}` +
            `  }${NL}` +
            `${ReplaceToken}`;
        code = code.replace(`${ReplaceToken}`, innerCode);
    }
}

export class ResolverFieldEntry extends BaseEntry implements IResolverEntry {
    constructor(entry: any) {
        super(entry);
    }

    genCode(parent: string): void {
        const fr = this.f();
        const beforeArgs = fr.args === '' ? '' : `, `;
        const args = (beforeArgs + fr.args).replace(/@Args/g, `${NL}${TABS}@Args`);
        const argsRealType = fr.argsRealType.replace(/, {/g, `,${NL}  //         {`);
        const innerCode =
            `${NL}` +
            `  // Args:   ${argsRealType}${NL}` +
            `  // Return: ${this.entry.types}${NL}` +
            `  @ResolveField('${this.entry.name}')${NL}` +
            useDurationInterceptor +
            `  async ${this.entry.name}(@Parent() parent: any${args}) {${NL}` +
            `${NL}` +
            `    return [];  //@@${NL}` +
            `  }${NL}` +
            `${ReplaceToken}`;
        code = code.replace(`${ReplaceToken}`, innerCode);
    }
}

export class Resolver {
    entries = new Array<IResolverEntry>();

    constructor(private name: string) { }

    addEntry(entry: IResolverEntry) {
        this.entries.push(entry);
    }

    genCode() {
        code +=
            `@Resolver('${this.name}')${NL}` +
            `export class ${this.name}${suffixResolverClass} ` +
            `extends BaseResolver<SqlService, NeoService> {${NL}` +
            `  constructor(sqlService: SqlService, neoService: NeoService) {${NL}` +
            `    super(sqlService, neoService);${NL}` +
            `  }${NL}` +
            `${ReplaceToken}` +
            `}${NL}`;

        for (let entry of this.entries)
            entry.genCode(this.name);

        code = code.replace(`${ReplaceToken}`, '');
        code += `${NL}`;
    }

    static endGen(resolverNames: string[]): string {
        const head =
            '// Template for this file was automatically generated according to graphQL schema.' + `${NL}` +
            '// The following block is subject to replacement on module runtime upload'  + `${NL}` +
            '// and should be maintained unchanged (including blanks).'  + `${NL}` +
            `${NL}` +
            '// {'  + `${NL}` +
            'const isFromWeb = false;' + `${NL}` +
            'let typePaths = [];' + `${NL}` +
            'let path = \'\';' + `${NL}` +
            '__dirname = \'\';' + `${NL}` +
            '// }'  + `${NL}` +
            `${NL}` +

            'if (isFromWeb)' + `${NL}` +
            '  process.chdir(__dirname);' + `${NL}` +
            `${NL}` +

            'import { Module, UseInterceptors, UseGuards } from \'../../node_modules/@nestjs/common\';' + `${NL}` +
            'import {' + `${NL}` +
            '  Resolver,'  + `${NL}` +
            '  Query,' + `${NL}` +
            '  Mutation' + `${NL}` +
            '  Args,' + `${NL}` +
            '  ResolveField,'  + `${NL}` +
            '  Parent,' + `${NL}` +
            '  Context,' + `${NL}` +
            '} from \'../../node_modules/@nestjs/graphql\';' + `${NL}` +
            'import { ConfigService } from \'../../node_modules/config-lib\';' + `${NL}` +
            'import { BaseResolver } from \'../base-classes/base.resolver\';' + `${NL}` +
            'import { SqlModule } from \'./sql/sql.module\';' + `${NL}` +
            'import { NeoModule } from \'./neo/neo.module\';' + `${NL}` +
            'import { NeoService } from \'./neo/neo.service\';' + `${NL}` +
            'import { SqlService } from \'./sql/sql.service\';' + `${NL}` +
            'import { ExecutionContextValidator } from \'../common/execution-context-validator\';' + `${NL}` +
            'import { logger } from \'../../node_modules/logger-lib\';' + `${NL}` +
            'import { AuthModule, GqlAuthGuard } from \'../../node_modules/auth-lib\';' + `${NL}` +
            'import { DirHolder } from \'../modules-tools/dir-holder\';' + `${NL}` +
            'const { ' +
            '  ExecutionContextValidationInterceptor,' + `${NL}` +
            '  DurationInterceptor,' + `${NL}` +
            '  BaseExecutionContextValidator,' + `${NL}` +
            '  TlsGuard' + `${NL}` +
            '} = require(\'../../node_modules/interceptors-lib\');' + `${NL}` +
            `${NL}` +

            'if (isFromWeb) {' + `${NL}` +
            '  process.chdir(DirHolder.getProjectDir());' + `${NL}` +
            'else {' + `${NL}` +
            '  const configService = new ConfigService();' + `${NL}` +
            '  const urlJoin = require(\'url-join\');' + `${NL}` +
            '  typePaths = [urlJoin(configService.get(\'GQL_URL\'), configService.get(\'GQL_SCHEMA\'))];' + `${NL}` +
            '  path = configService.get(\'GQL_PATH\');' + `${NL}` +
            '}' + `${NL}` +
            `${NL}` +

            'const gql_module_lib = \'gql-module-lib\';' + `${NL}` +
            'const { GraphQLModuleEx } = require(isFromWeb ? `./node_modules/${gql_module_lib}` : gql_module_lib);' + `${NL}` +
            `${NL}` +
            '///////////////////////////////////////////////////////////////////////////////////////////////////////' +
            `${NL}` + `${NL}` + `${NL}`;

        const tail =
            '@Module({' + `${NL}` +
            '  imports: [' + `${NL}` +
            '    GraphQLModuleEx.forRoot({' + `${NL}` +
            '      debug: false,' + `${NL}` +
            '      playground: true,' + `${NL}` +
            '      typePaths,' + `${NL}` +
            '      path,' + `${NL}` +
            '      context: ({ req }) => ({ req }),' + `${NL}` +
            '    }),' + `${NL}` +
            '    AuthModule,' + `${NL}` +
            '    SqlModule,' + `${NL}` +
            '    NeoModule' + `${NL}` +
            '  ],' + `${NL}` +
            `  providers: [${NL}${TABS}${ReplaceToken}` + `${NL}` +
            '})' + `${NL}` +
            'export class GqlModule {' + `${NL}` +
            '  constructor() {' + `${NL}` +
            '    logger.log(\'GqlModule has been created\');' + `${NL}` +
            '  }' + `${NL}` +
            `${NL}` +
            '  onModuleInit() {' + `${NL}` +
            '    logger.log(\'GqlModule has been initialized\');' + `${NL}` +
            '  }' + `${NL}` +
            '}' + `${NL}` +
            `${NL}` +
            'export function getModule() { return GqlModule; }' + `${NL}`;

        let str = '';
        for (let s of resolverNames)
            str += s + `${suffixResolverClass},${NL}${TABS}`;
        str += ']';

        return head +
            code.replace(`${ReplaceToken}`, '') +
            tail.replace(`${ReplaceToken}`, `${str}`);
    }
}

